package src.actors;

import jTiled2D.Config;
import jTiled2D.entidades.Entidade;
import jTiled2D.gfx.EntidadeSprite;

public class NPC extends Entidade {
    private String tipoMovimento;
    private String dialogo;
    private int ticksParaMover;

    public NPC(float x, float y, EntidadeSprite es, String tipoMovimento) throws Exception {
        super(x, y, es);
        this.tipoMovimento = tipoMovimento;
    }

    @Override
    public void tick() {
        if (!movendo)
            MovimentoNPC();
        super.tick();
    }

    private void MovimentoNPC() {
        //Aguardar 30 ticks ate mover novamente
        if(ticksParaMover == 0) {
            ticksParaMover = 30;
            movendo = false;
            if (tipoMovimento.equals("random")) {
                moverAleatorio();
            }
        } else{
            ticksParaMover --;
        }
    }



    public void setDialogo(String dialogo) {
        this.dialogo = dialogo;
    }

    public String getDialogo() {
        return this.dialogo;
    }

}
