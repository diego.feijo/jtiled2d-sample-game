package src.gfx;

import java.io.InputStream;

public class Resources {

    public final String windowskin = getClass().getResource("windowskin.png").getPath();
    public final String mapa1 = getClass().getResource("testMap.tmx").getPath();
    public final String characters = getClass().getResource("characters.png").getPath();

    public final InputStream tileset = getClass().getResourceAsStream("indoor.png");

}
