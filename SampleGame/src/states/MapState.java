package src.states;

import src.actors.Enemy;
import src.actors.NPC;
import src.actors.Player;
import src.gfx.Resources;
import jTiled2D.Config;
import jTiled2D.gfx.EntidadeSprite;
import jTiled2D.gfx.FolhaSprite;
import jTiled2D.janelas.JanelaBase;
import jTiled2D.janelas.JanelaMessage;
import jTiled2D.mapas.Mapa;
import jTiled2D.mapas.MapaObjeto;
import jTiled2D.mapas.TiledReader;
import jTiled2D.estados.Estado;

import java.util.ArrayList;
import java.util.List;

import java.awt.*;

public class MapState extends Estado {

    private Mapa mapa;
    private Player player;
    private JanelaMessage janelaMessage;
    private List<NPC> npcs;
    private List<Enemy> enemies;

    //Controla a exibição de diálogos
    private boolean emDialogo;

    MapState(String path) {
        Config.getInstance().setTilesetAtual(new Resources().tileset);
        this.mapa = TiledReader.lerArquivo(path);
        init();
    }

    @Override
    public void init() {
        EntidadeSprite es = new EntidadeSprite(new Resources().characters, 100, FolhaSprite.PosSprite.POS7);
        try {
            player = new Player(12, 5, es);
            Config.getInstance().getGameCamera().centralizaEm(player);
            mapa.getEntidadeGerenciador().adicionaEntidade(player);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Inicializa a janela de mensagem
        janelaMessage = new JanelaMessage(JanelaBase.AlinhamentoVertical.ABAIXO, 100);

        populaNPCs();
        populaInimigos();
    }

    private void populaNPCs() {
        //Confere os objetos do mapa, se temos algum NPC
        List<MapaObjeto> moNPCs = this.mapa.getConfig().getObjetoPorNome("npc");
        if (moNPCs != null && !moNPCs.isEmpty()) {
            this.npcs = new ArrayList<>();
            EntidadeSprite npcES = new EntidadeSprite(new Resources().characters, 100, FolhaSprite.PosSprite.POS2);
            for (MapaObjeto mo : moNPCs) {
                String tipoMovimento = mo.getPropriedade("tipoMovimento");
                String dialogo = mo.getPropriedade("dialogo");
                try {
                    NPC npc = new NPC(mo.getPosX(), mo.getPosY(), npcES, tipoMovimento);
                    if (dialogo != null && !dialogo.trim().isEmpty())
                        npc.setDialogo(dialogo);
                    this.npcs.add(npc);
                    this.mapa.getEntidadeGerenciador().adicionaEntidade(npc);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void populaInimigos() {
        //Confere os objetos do mapa, se temos algum NPC
        List<MapaObjeto> moInimigos = this.mapa.getConfig().getObjetoPorNome("enemy");
        if (moInimigos != null && !moInimigos.isEmpty()) {
            this.enemies = new ArrayList<>();
            EntidadeSprite npcES = new EntidadeSprite(new Resources().characters, 100, FolhaSprite.PosSprite.POS5);
            for (MapaObjeto mo : moInimigos) {
                String tipoMovimento = mo.getPropriedade("tipoMovimento");
                try {
                    Enemy enemy = new Enemy(mo.getPosX(), mo.getPosY(), npcES, tipoMovimento);
                    enemy.setSolido(false);
                    this.enemies.add(enemy);
                    this.mapa.getEntidadeGerenciador().adicionaEntidade(enemy);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void tick() {
        this.mapa.tick();
        if (emDialogo) {
            this.mapa.getEntidadeGerenciador().desativarEntidades();
            this.mapa.getEntidadeGerenciador().desativarAcaoEntidades();
            if (janelaMessage.isVisivel()) {
                janelaMessage.tick();
            } else {
                emDialogo = false;
            }
        } else {
            this.mapa.getEntidadeGerenciador().ativarEntidades();
            if (player != null) {
                Config.getInstance().getGameCamera().centralizaEm(player);
            }
            //Confere se algum NPC tem algo a dizer
            confereAcaoNPC();
            confereAcaoInimigo();
        }
    }

    @Override
    public void render(Graphics g) {
        this.mapa.render(g);
        if (player != null) {
            player.render(g);
        }
        if (emDialogo)
            janelaMessage.render(g);
    }

    private void confereAcaoNPC() {
        if (npcs != null && !npcs.isEmpty()) {
            //Confere se algum NPC esta na mesma posicao do jogador
            for (NPC npc : npcs) {
                //Se esse carinha nao tem nada a dizer, nada sera dito
                if (npc.acaoDisparada) {
                    if (npc.getDialogo() == null || npc.getDialogo().isEmpty())
                        npc.acaoDisparada = false;
                    else {
                        janelaMessage.setVisivel(true);
                        janelaMessage.setTexto(npc.getDialogo());
                        emDialogo = true;
                    }
                }
            }
        }
    }

    private void confereAcaoInimigo() {
        if (enemies != null && !enemies.isEmpty()) {
            //Confere se algum Inimigo esta na mesma posicao do jogador
            for (Enemy enemy : enemies) {
                if(!enemy.ativo)
                    continue;
                //Se houve colisão com o jogador
                if(enemy.getTileX() == player.getTileX() && enemy.getTileY() == player.getTileY()){
                    setEstado(new MenuState());
                } else if (enemy.acaoDisparada) {
                    mapa.getEntidadeGerenciador().removeEntidade(enemy);
                    enemy.ativo = false;
                }
            }
        }
    }
}
