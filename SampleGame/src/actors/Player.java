package src.actors;

import jTiled2D.Config;
import jTiled2D.entidades.Entidade;
import jTiled2D.gfx.EntidadeSprite;

public class Player extends Entidade {

    public Player(int x, int y, EntidadeSprite es) throws Exception {
        super(x * Config.getInstance().getLarguraTile(),
                y * Config.getInstance().getAlturaTile(),
                es);
    }

    @Override
    public void tick() {
        getInput();
        super.tick();
    }

    @Override
    protected void disparaAcao() {
        //Confere se tem alguma entidade adjacente para interagir
        int eX = this.getTileX();
        int eY = this.getTileY();
        switch (dir) {
            case CIMA:
                eY -= 1;
                break;
            case BAIXO:
                eY += 1;
                break;
            case DIREITA:
                eX += 1;
                break;
            case ESQUERDA:
                eX -= 1;
                break;
        }
        Entidade e = mapa.getEntity(eX, eY);
        if (e != null)
            e.acaoDisparada = true;
    }

}
