package src.states;

import jTiled2D.estados.Estado;
import jTiled2D.janelas.JanelaBase;
import jTiled2D.janelas.JanelaBotaoMenu;
import src.gfx.Resources;

import java.awt.*;

public class MenuState extends Estado {

    private boolean init;
    private JanelaBotaoMenu menu;
    private Resources resources;

    public MenuState() {
        resources = new Resources();
    }

    @Override
    public void init() {
        menu = new JanelaBotaoMenu(new String[]{"Inciar", "Sair"});
        menu.setAlinhamentoVertical(JanelaBase.AlinhamentoVertical.MEIO);
        menu.setAlinhamentoHorizontal(JanelaBase.AlinhamentoHorizontal.CENTRO);
        init = true;
    }

    @Override
    public void tick() {
        if (init) {
            switch (menu.getIndice()) {
                case 0: //iniciar
                    setEstado(new MapState(resources.mapa1));
                    break;
                case 1: //sair
                    System.exit(0);
                    break;
            }
        }
    }

    @Override
    public void render(Graphics g) {
        if (!init) init();
        menu.render(g);
    }
}
