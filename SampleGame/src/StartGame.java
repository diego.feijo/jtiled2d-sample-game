package src;

import src.states.MenuState;
import src.gfx.Resources;
import jTiled2D.Config;
import jTiled2D.Launcher;

import java.awt.*;

public class StartGame {
    public static void main(String[] args) {
        try {
            Resources resources = new Resources();
            Config.getInstance().setFontePadrao(new Font("Tahoma", Font.PLAIN, 22));
            Config.getInstance().setCorPadrao(Color.WHITE);
            Launcher.launchGame("Sample Game", 800, 608,
                    30, resources.windowskin, new MenuState());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
